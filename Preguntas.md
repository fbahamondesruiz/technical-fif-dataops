# <p style="text-align: center;">Prueba Técnica FIF DevOps Cloud</p>

## Preguntas

1. Se tiene un pod dentro de un GKE el cual se tiene previsto que dentro de unos días realizará un alto consumo de recursos. El pod está dentro de un pool de nodos separado del principal. ¿Cómo es posible escalar el pod para que logre manejar el alto consumo? ¿Hay factibilidad de scale down?

> R: En caso que no sepamos cuanto será el consumo dentro de los próximos días y desconozcamos su duración, yo utilizaría el servicio Horizontal Pod Autoscaling, que nos permite habilitar pods cuando se cumplan uso de carga en los pods existentes y cuando no se estén utilizando, se disminuye la cantidad de pods, permitiendo así el scale down y así pagar solo lo que utilizamos.

___

2. Para el mismo endpoint de la parte 1 ahora se necesita restringir el acceso vía IAM de GCP. ¿Cuál o cuáles soluciones podrían utilizarse?

> R: Se puede solicitar acceso a un endpoint a través api keys, de tal manera que se pueda controlar el uso de los distintos metodos que disponga y bloquear todos o algunos de ellos (get, post, put, delete). 

___

3. ¿Cuál es la diferencia entre un Service, Ingress, NodePort y clusterIP? ¿En qué casos es necesario uno sobre el otro?

> R: 
> - Service: Un servicio proporciona conectividad entre los pods de app y los demás servicios del clúster, sin exponer la dirección IP Privada de cada pod. Es accesible desde una IP Pública.
> - NodePort: Cuando se levanta una app, estas quedan expuestas en los rangos de puertos 30000 a 32767. Debido a que este tipo de red no expone una IP Pública, la única manera de acceder es con una VPN.
> - ClusterIp: Solo se puede exponer apps en la red privada.
> - Ingress: Este tipo de red hereda muchas de las caracteristicas de otros tipos, con la diferencia que nos permite crear reglas de direccionamiento personalizadas y también tener varias apps por servicio.

___

4. Se tiene un cluster K8S on-premise. ¿Cómo conectarías este cluster con uno en GCP (GKE) para interconectar ambos cluster que se usan en distintas partes de una arquitectura sin exponer API REST? (explique más de una opción, siendo al menos una que no contemple una solución a nivel de redes).

> R: Una opción sería Google Cloud API Gateway, que es un servicio serverless que facilita la administración, mantenimiento y administración de las API. La solución consiste en GCP Cloud API Gateway es una interfaz que se encargará de procesar las solicitudes entrantes y distribuir hacia los backends integrados. Google Cloud posee otras alternativas de conectividad de red bajo el nombre de Google Cloud Hybrid Connectivity, estas consisten en 3 servicios: Cloud VPN, Partner Interconnect y Dedicated Interconnect, siendo estas 2 últimas enlaces de red instalados en sus ubicaciones.

___

5. ¿Cuáles son buenos criterios a la hora de escoger Serverless o VMs/K8S para un sistema?. Da **al menos 2** ejemplos

> R: Un criterio podría ser el tipo de aplicación que queremos levantar, ya que las aplicaciones complejas pueden ser más dificiles de construir en Serverless. Y otro criterio sería la administración y mantenimiento que estamos dispuestos a disponibilizar, ya que el serverless reduce bastante la administración y el mantenimiento. Otro punto importante es la escalabilidad del servidor. 

___

6. En un producto “X” que se despliega en un Kubernetes se solicita utilizar una base de datos SQL con persistencia para elementos transaccionales. Estos elementos no pueden perderse, aunque estos estén en un middleware. ¿Cómo desplegaría ud dicha base de datos? ¿Cómo ésta se persistiría y aseguraría alta disponibilidad?. Justifique su elección.

> R: Levantaría una base de datos Cloud SQL en GCP, ya que es una base de alta disponibilidad, baja latencia y ofrecen recuperación de datos en cualquier momento. Se puede conectar desde un kubernetes o cualquier servicio de compute engine de gcp

___

7. A usted le dan el requerimiento de disponibilizar un producto a los clientes, el cual está asociado a una imagen de contenedor. Este producto debe estar disponible para una gran cantidad de clientes de forma concurrente, como un retail en cyberday ¿Qué tecnologías utilizaría para asegurar la disponibilidad? Considerar todo el ciclo de DevOps

> R: Utilizaría un cluster con nodos mínimos y máximos, para manejar los picos de carga, junto con un balanceador para responder a todas las solicitudes.

___